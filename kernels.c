/********************************************************
 * Kernels to be optimized for the CS:APP Performance Lab
 ********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "defs.h"

/* 
 * Please fill in the following team struct 
 */
team_t team = {
    "shpitzy1",              /* Team name */

    "Yair Shpitzer",     /* First member full name */
    "yairsh39@gmail.com",  /* First member email address */

    "",                   /* Second member full name (leave blank if none) */
    ""                    /* Second member email addr (leave blank if none) */
};

/***************
 * ROTATE KERNEL
 ***************/

/******************************************************
 * Your different versions of the rotate kernel go here
 ******************************************************/

/* 
 * naive_rotate - The naive baseline version of rotate 
 */
char naive_rotate_descr[] = "naive_rotate: Naive baseline implementation";
void naive_rotate(int dim, pixel *src, pixel *dst) 
{
    int i, j;

    for (i = 0; i < dim; i++)
	for (j = 0; j < dim; j++)
	    dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)];
}

/* 
 * rotate - Your current working version of rotate
 * IMPORTANT: This is the version you will be graded on
 */
char rotate_descr[] = "rotate: Current working version";
void rotate(int dim, pixel *src, pixel *dst)
{
    //naive_rotate(dim, src, dst);

    int block_size = 32;
    int i, j;
    int k, l;
    for(i = 0; i < dim; i += block_size) {
        for(j = 0; j < dim; j += block_size) {
            for (k = i; k < i + block_size; ++k) {
                for (l = j; l < j + block_size; ++l) {
                    dst[RIDX(dim-1-k, l, dim)] = src[RIDX(l, k, dim)];
                }
            }
        }
    }

   /* for(i = 0; i < dim; i += block_size)
        for(j = 0; j < dim; j += block_size)
            for(k = i; k < i + block_size; ++k)
                for(l = j; l < j + block_size; ++l)
                    dst[(dim - 1 - l) * dim + k] = src[k * dim + l];*/
}

/*********************************************************************
 * register_rotate_functions - Register all of your different versions
 *     of the rotate kernel with the driver by calling the
 *     add_rotate_function() for each test function. When you run the
 *     driver program, it will test and report the performance of each
 *     registered test function.  
 *********************************************************************/

void register_rotate_functions() 
{
    add_rotate_function(&naive_rotate, naive_rotate_descr);   
    add_rotate_function(&rotate, rotate_descr);   
    /* ... Register additional test functions here */
}


/***************
 * SMOOTH KERNEL
 **************/

/***************************************************************
 * Various typedefs and helper functions for the smooth function
 * You may modify these any way you like.
 **************************************************************/

/* A struct used to compute averaged pixel value */
typedef struct {
    int red;
    int green;
    int blue;
    int num;
} pixel_sum;

/* Compute min and max of two integers, respectively */
static int min(int a, int b) { return (a < b ? a : b); }
static int max(int a, int b) { return (a > b ? a : b); }

/* 
 * initialize_pixel_sum - Initializes all fields of sum to 0 
 */
static void initialize_pixel_sum(pixel_sum *sum) 
{
    sum->red = sum->green = sum->blue = 0;
    sum->num = 0;
    return;
}

/* 
 * accumulate_sum - Accumulates field values of p in corresponding 
 * fields of sum 
 */
static void accumulate_sum(pixel_sum *sum, pixel p)
{
    sum->red += (int) p.red;
    sum->green += (int) p.green;
    sum->blue += (int) p.blue;
    sum->num++;
    return;
}

/* 
 * assign_sum_to_pixel - Computes averaged pixel value in current_pixel 
 */
static void assign_sum_to_pixel(pixel *current_pixel, pixel_sum sum) 
{
    current_pixel->red = (unsigned short) (sum.red/sum.num);
    current_pixel->green = (unsigned short) (sum.green/sum.num);
    current_pixel->blue = (unsigned short) (sum.blue/sum.num);
    return;
}

/* 
 * avg - Returns averaged pixel value at (i,j) 
 */
static pixel avg(int dim, int i, int j, pixel *src) 
{
    int ii, jj;
    pixel_sum sum;
    pixel current_pixel;

    initialize_pixel_sum(&sum);
    for(ii = max(i-1, 0); ii <= min(i+1, dim-1); ii++) 
	for(jj = max(j-1, 0); jj <= min(j+1, dim-1); jj++) 
	    accumulate_sum(&sum, src[RIDX(ii, jj, dim)]);

    assign_sum_to_pixel(&current_pixel, sum);
    return current_pixel;
}

/******************************************************
 * Your different versions of the smooth kernel go here
 ******************************************************/


/*
 * naive_smooth - The naive baseline version of smooth 
 */
char naive_smooth_descr[] = "naive_smooth: Naive baseline implementation";
void naive_smooth(int dim, pixel *src, pixel *dst) 
{
    int i, j;

    for (i = 0; i < dim; i++)
	    for (j = 0; j < dim; j++) {
            dst[RIDX(i, j, dim)] = avg(dim, i, j, src);
           // printf("dst[%d]: red - %d, green - %d, blue - %d\n",RIDX(i, j, dim), dst[RIDX(i, j, dim)].red, dst[RIDX(i, j, dim)].green, dst[RIDX(i, j, dim)].blue);
        }
}

/*
 * smooth - Your current working version of smooth. 
 * IMPORTANT: This is the version you will be graded on
 */
char smooth_descr[] = "smooth: Current working version";
void smooth(int dim, pixel *src, pixel *dst) 
{
    int sumRed, sumGreen, sumBlue;

    // corners

    // top-left
    sumRed = (src[0].red + src[1].red) + (src[dim].red + src[dim + 1].red);
    sumGreen = (src[0].green + src[1].green) + (src[dim].green + src[dim + 1].green);
    sumBlue = (src[0].blue + src[1].blue) + (src[dim].blue + src[dim + 1].blue);
    dst[0].red = sumRed / 4;
    dst[0].green = sumGreen / 4;
    dst[0].blue = sumBlue / 4;

    // top-right
    int index = RIDX(0, dim - 1, dim);
    sumRed = (src[index].red + src[index - 1].red) + (src[index + dim].red + src[index + dim - 1].red);
    sumGreen = (src[index].green + src[index - 1].green) + (src[index + dim].green + src[index + dim - 1].green);
    sumBlue = (src[index].blue + src[index - 1].blue) + (src[index + dim].blue + src[index + dim - 1].blue);
    dst[index].red = sumRed / 4;
    dst[index].green = sumGreen / 4;
    dst[index].blue = sumBlue / 4;

    // bottom-left
    index = RIDX(dim - 1, 0, dim);
    sumRed = (src[index].red + src[index + 1].red) + (src[index - dim].red + src[index - dim + 1].red);
    sumGreen = (src[index].green + src[index + 1].green) + (src[index - dim].green + src[index - dim + 1].green);
    sumBlue = (src[index].blue + src[index + 1].blue) + (src[index - dim].blue + src[index - dim + 1].blue);
    dst[index].red = sumRed / 4;
    dst[index].green = sumGreen / 4;
    dst[index].blue = sumBlue / 4;

    // bottom-right
    index = RIDX(dim - 1, dim - 1, dim);
    sumRed = src[index].red + src[index - 1].red + src[index - dim].red + src[index - dim - 1].red;
    sumGreen = (src[index].green + src[index - 1].green) + (src[index - dim].green + src[index - dim - 1].green);
    sumBlue = (src[index].blue + src[index - 1].blue) + (src[index - dim].blue + src[index - dim - 1].blue);
    dst[index].red = sumRed / 4;
    dst[index].green = sumGreen / 4;
    dst[index].blue = sumBlue / 4;


    // frame
    int i, j;

    // up
    for (j = 1; j < dim - 1; j++){
        index = RIDX(0, j, dim);
        sumRed = src[index - 1].red + src[index].red +
                 src[index + 1].red + src[index + dim - 1].red +
                 src[index + dim].red + src[index + dim + 1].red;
        sumGreen = src[index - 1].green + src[index].green +
                   src[index + 1].green + src[index + dim - 1].green +
                   src[index + dim].green + src[index + dim + 1].green;
        sumBlue = src[index - 1].blue + src[index].blue +
                  src[index + 1].blue + src[index + dim - 1].blue +
                  src[index + dim].blue + src[index + dim + 1].blue;
        dst[index].red = sumRed / 6;
        dst[index].green = sumGreen / 6;
        dst[index].blue = sumBlue / 6;
    }

    // down
    for (j = 1; j < dim - 1; j++){
        index = RIDX(dim - 1, j, dim);
        sumRed = src[index - 1].red + src[index].red +
                 src[index + 1].red + src[index - dim - 1].red +
                 src[index - dim].red + src[index - dim + 1].red;
        sumGreen = src[index - 1].green + src[index].green +
                   src[index + 1].green + src[index - dim - 1].green +
                   src[index - dim].green + src[index - dim + 1].green;
        sumBlue = src[index - 1].blue + src[index].blue +
                  src[index + 1].blue + src[index - dim - 1].blue +
                  src[index - dim].blue + src[index - dim + 1].blue;
        dst[index].red = sumRed / 6;
        dst[index].green = sumGreen / 6;
        dst[index].blue = sumBlue / 6;
    }

    // left
    for (i = 1; i < dim - 1; i++){
        index = RIDX(i, 0, dim);
        sumRed = src[index - dim].red + src[index - dim + 1].red +
                 src[index].red + src[index + 1].red +
                 src[index + dim].red + src[index + dim + 1].red;
        sumGreen = src[index - dim].green + src[index - dim + 1].green +
                   src[index].green + src[index + 1].green +
                   src[index + dim].green + src[index + dim + 1].green;
        sumBlue = src[index - dim].blue + src[index - dim + 1].blue +
                  src[index].blue + src[index + 1].blue +
                  src[index + dim].blue + src[index + dim + 1].blue;
        dst[index].red = sumRed / 6;
        dst[index].green = sumGreen / 6;
        dst[index].blue = sumBlue / 6;
    }

    // right
    for (i = 1; i < dim - 1; i++){
        index = RIDX(i, dim - 1, dim);
        sumRed = src[index - dim].red + src[index - dim - 1].red +
                 src[index].red + src[index - 1].red +
                 src[index + dim].red + src[index + dim - 1].red;
        sumGreen = src[index - dim].green + src[index - dim - 1].green +
                   src[index].green + src[index - 1].green +
                   src[index + dim].green + src[index + dim - 1].green;
        sumBlue = src[index - dim].blue + src[index - dim - 1].blue +
                  src[index].blue + src[index - 1].blue +
                  src[index + dim].blue + src[index + dim - 1].blue;
        dst[index].red = sumRed / 6;
        dst[index].green = sumGreen / 6;
        dst[index].blue = sumBlue / 6;
    }

    for (i = 1; i < dim - 1; i++){
        for (j = 1; j < dim - 1; j += 2){
            index = RIDX(i, j, dim);

            sumRed = src[index - dim - 1].red + src[index - dim].red + src[index - dim + 1].red +
                     src[index - 1].red + src[index].red + src[index + 1].red +
                     src[index + dim - 1].red + src[index + dim].red + src[index + dim + 1].red;
            sumGreen = src[index - dim - 1].green + src[index - dim].green + src[index - dim + 1].green +
                       src[index - 1].green + src[index].green + src[index + 1].green +
                       src[index + dim - 1].green + src[index + dim].green + src[index + dim + 1].green;
            sumBlue = src[index - dim - 1].blue + src[index - dim].blue + src[index - dim + 1].blue +
                      src[index - 1].blue + src[index].blue + src[index + 1].blue +
                      src[index + dim - 1].blue + src[index + dim].blue + src[index + dim + 1].blue;
            dst[index].red = sumRed / 9;
            dst[index].green = sumGreen / 9;
            dst[index].blue = sumBlue / 9;

            ++index;
            sumRed = src[index - dim - 1].red + src[index - dim].red + src[index - dim + 1].red +
                     src[index - 1].red + src[index].red + src[index + 1].red +
                     src[index + dim - 1].red + src[index + dim].red + src[index + dim + 1].red;
            sumGreen = src[index - dim - 1].green + src[index - dim].green + src[index - dim + 1].green +
                       src[index - 1].green + src[index].green + src[index + 1].green +
                       src[index + dim - 1].green + src[index + dim].green + src[index + dim + 1].green;
            sumBlue = src[index - dim - 1].blue + src[index - dim].blue + src[index - dim + 1].blue +
                      src[index - 1].blue + src[index].blue + src[index + 1].blue +
                      src[index + dim - 1].blue + src[index + dim].blue + src[index + dim + 1].blue;
            dst[index].red = sumRed / 9;
            dst[index].green = sumGreen / 9;
            dst[index].blue = sumBlue / 9;
        }
    }

}


/********************************************************************* 
 * register_smooth_functions - Register all of your different versions
 *     of the smooth kernel with the driver by calling the
 *     add_smooth_function() for each test function.  When you run the
 *     driver program, it will test and report the performance of each
 *     registered test function.  
 *********************************************************************/

void register_smooth_functions() {
    add_smooth_function(&smooth, smooth_descr);
    add_smooth_function(&naive_smooth, naive_smooth_descr);
    /* ... Register additional test functions here */
}

